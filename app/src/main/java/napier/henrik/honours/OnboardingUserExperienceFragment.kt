package napier.henrik.honours

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RadioGroup
import android.widget.SeekBar
import android.widget.TextView
import android.widget.ToggleButton

class OnboardingUserExperienceFragment : OnboardingFragment() {
    private var recyclerView: RecyclerView? = null
    private lateinit var viewAdapter: RecyclerView.Adapter<*>
    private lateinit var viewManager: LinearLayoutManager
    private lateinit var maxLifts: ArrayList<Pair<Int, Float>>

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val rootView = inflater.inflate(R.layout.onboarding_user_experience, container, false)

        val exBundle = arguments
        val exArrayList = exBundle?.getParcelableArrayList<DataWrangler.ExerciseJSON>("EXERCISES")
//        user = exBundle?.getParcelable<UserData>("USER")
        var exArray = exArrayList?.map { it.fields.name }
        exArray = exArray!!.sorted()

        viewManager = LinearLayoutManager(context)
        if (exArray != null) {
            maxLifts = ArrayList()
            viewAdapter = AdapterMaxLift(maxLifts, exArray.toTypedArray(), true)
        }

        recyclerView = rootView.findViewById<RecyclerView>(R.id.obMaxLiftRecycler)
        recyclerView?.layoutManager = viewManager
        recyclerView?.adapter = viewAdapter

        var goalToggle: ToggleButton = rootView.findViewById(R.id.goalToggleButton)
        goalToggle.textOff = "Strength"
        goalToggle.text = "Strength"
        goalToggle.textOn = "Hypertrophy"

        var goalText: TextView = rootView.findViewById(R.id.goalTextView)

        var radioGroup: RadioGroup = rootView.findViewById(R.id.experienceLevelRadioGroup)
        radioGroup.setOnCheckedChangeListener { radioGroupSelected, i ->
            when (radioGroup.checkedRadioButtonId) {
                R.id.intermediateRadioButton, R.id.advancedRadioButton -> {
                    goalToggle.visibility = View.VISIBLE
                    goalText.visibility = View.VISIBLE
                }
                else -> {
                    goalToggle.visibility = View.INVISIBLE
                    goalText.visibility = View.INVISIBLE
                }
            }
        }
        return rootView
    }

    override fun validate(): Boolean {
        var radioGroup: RadioGroup = view!!.findViewById(R.id.experienceLevelRadioGroup)
        var days: SeekBar = view!!.findViewById(R.id.daysPerWeekSeekBar)
        var goalToggle: ToggleButton = view!!.findViewById(R.id.goalToggleButton)
        var exp = 0
        when (radioGroup.checkedRadioButtonId) {
            R.id.beginnerRadioButton -> exp = 1
            R.id.intermediateRadioButton -> {
                exp = 2
            }
            R.id.advancedRadioButton -> {
                exp = 3
            }
        }
        if (exp > 0 && days.progress > 0) {
            mCallback.onDataPass(mapOf("EXPERIENCE" to exp, "DAYS" to days.progress, "MAX" to maxLifts, "GOAL" to goalToggle.text.toString()))
            return true
        }
        return false
    }


}