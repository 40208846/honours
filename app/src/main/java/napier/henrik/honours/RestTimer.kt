package napier.henrik.honours

import android.os.CountDownTimer
import android.os.VibrationEffect
import android.os.Vibrator
import android.widget.TextView
import java.util.*
import java.util.concurrent.TimeUnit


class RestTimer {
    companion object {
        lateinit var currentTimer: CountDownTimer
        var currDuration = 0L
    }
//    lateinit var vibrator: Vibrator

    fun setTimer(duration: Long, textView: TextView, vibrator: Vibrator) {
        currentTimer = object : CountDownTimer(duration, 1000) {
            override fun onTick(millisUntilFinished: Long) {
                currDuration = millisUntilFinished
                var millisUntilFinished: Long = millisUntilFinished
                val minutes = TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished)
                millisUntilFinished -= TimeUnit.MINUTES.toMillis(minutes)
                val seconds = TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished)
                textView.text = String.format(Locale.getDefault(), "%02d:%02d", minutes, seconds)
            }

            override fun onFinish() {
                vibrator.vibrate(VibrationEffect.createOneShot(500, VibrationEffect.DEFAULT_AMPLITUDE))
                textView.text = "NEXT"
            }
        }
    }

    fun setTimer(textView: TextView, vibrator: Vibrator) {
        currentTimer = object : CountDownTimer(currDuration, 1000) {
            override fun onTick(millisUntilFinished: Long) {
                currDuration = millisUntilFinished
                var millisUntilFinished: Long = millisUntilFinished
                val minutes = TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished)
                millisUntilFinished -= TimeUnit.MINUTES.toMillis(minutes)
                val seconds = TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished)
                textView.text = String.format(Locale.getDefault(), "%02d:%02d", minutes, seconds)
            }

            override fun onFinish() {
                vibrator.vibrate(VibrationEffect.createOneShot(500, VibrationEffect.DEFAULT_AMPLITUDE))
                textView.text = "NEXT"
            }

        }
    }
}
