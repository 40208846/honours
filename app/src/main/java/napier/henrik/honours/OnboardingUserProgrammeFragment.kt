package napier.henrik.honours

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView

class OnboardingUserProgrammeFragment : OnboardingFragment() {
    private lateinit var recyclerView: RecyclerView
    private lateinit var viewAdapter: RecyclerView.Adapter<*>
    private lateinit var viewManager: LinearLayoutManager
    private lateinit var programmeNameTextView: TextView
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val rootView = inflater.inflate(R.layout.onboarding_user_programme, container, false)
        viewAdapter = AdapterSession(arrayListOf())
        viewManager = LinearLayoutManager(context)
        recyclerView = rootView.findViewById(R.id.previewRecycler)
        recyclerView.layoutManager = viewManager
        recyclerView.adapter = viewAdapter

        programmeNameTextView = rootView.findViewById(R.id.programmeNameTextView)

        return rootView
    }

    fun showProgramme(userData: UserData) {
        programmeNameTextView.text = userData.programme!!.programme.name
        viewAdapter = AdapterSession(userData.programme!!.programme.sessions)
        recyclerView.adapter = viewAdapter

    }
}
