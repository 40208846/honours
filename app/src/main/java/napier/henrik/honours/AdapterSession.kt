package napier.henrik.honours

import android.support.v7.widget.CardView
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView

class AdapterSession(private val myDataSet: ArrayList<Session>) : RecyclerView.Adapter<AdapterSession.SessionViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SessionViewHolder {
        val cardView = LayoutInflater.from(parent.context).inflate(R.layout.session_preview, parent, false) as CardView
        return SessionViewHolder(cardView)
    }

    override fun onBindViewHolder(holder: SessionViewHolder, position: Int) {
        val sess: Session = myDataSet[position]
        var titleString = ""
        var descString = ""
        var cats = LinkedHashSet<Int>()

        for (exercise in sess.exercises) {
            if (UserManager.exercises[exercise]?.fields?.category != null) {
                cats.add(UserManager.exercises[exercise]!!.fields.category)
                descString = descString + UserManager.exercises[exercise]!!.fields.name + ", "
            }
        }

        for (cat in cats) {
            titleString = titleString + UserManager.categories[cat] + ", "
        }

        titleString = titleString.substring(0 until titleString.length - 2)
        descString = descString.substring(0 until descString.length - 2)
        holder.sessionTitle.text = titleString
        holder.sessionDesc.text = descString
        holder.itemView.setOnClickListener { v ->
            {
            }
        }
    }


    override fun getItemCount() = myDataSet.size

    class SessionViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private var view: View = itemView
        var sessionTitle: TextView = view.findViewById(R.id.sessPreviewTitle)
        var sessionDesc: TextView = view.findViewById(R.id.sessContentText)
//        var button: Button = view.findViewById(R.id.button)

    }
}
