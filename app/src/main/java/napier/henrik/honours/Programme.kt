package napier.henrik.honours

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize
import java.io.Serializable

// Goal: 0 Strength, 1 hypertrophy

@Parcelize
data class Programme(val name: String, val days: Int, var sessions: ArrayList<Session>, var goal: Int) : Parcelable, Serializable
