package napier.henrik.honours

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize
import java.io.Serializable
import java.util.*

@Parcelize
class SessionInst(var exerciseInsts: ArrayList<ExerciseInst>) : Parcelable, Serializable {

    var date: Date = Date(System.currentTimeMillis())

}
