package napier.henrik.honours

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize
import java.io.Serializable

// Holds information about one specific set, more types of information to be stored can be added
@Parcelize
class Set(var weight: Float, val repGoal: Int, var repsCompleted: Int) : Parcelable, Serializable
