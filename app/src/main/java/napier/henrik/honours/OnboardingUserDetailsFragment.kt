package napier.henrik.honours

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText

class OnboardingUserDetailsFragment : OnboardingFragment() {
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val rootView = inflater.inflate(R.layout.onboarding_user_details, container, false)

        return rootView
    }

    override fun validate(): Boolean {
        var userName: EditText = view!!.findViewById(R.id.nameEditText)
        var userAge: EditText = view!!.findViewById(R.id.ageEditText)
        var userWeight: EditText = view!!.findViewById(R.id.weightEditText)

        if (userName.text.toString() != "" && userAge.text.toString() != "" && userWeight.text.toString() != "") {
            mCallback.onDataPass(mapOf("NAME" to userName.text.toString(), "AGE" to userAge.text.toString().toInt(), "WEIGHT" to userWeight.text.toString().toFloat()))
            return true
        }
        return false
    }


}