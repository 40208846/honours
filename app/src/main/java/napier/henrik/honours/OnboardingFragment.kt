package napier.henrik.honours

import android.support.v4.app.Fragment

open class OnboardingFragment : Fragment() {
    lateinit var mCallback: OnDataPass

    interface OnDataPass {
        fun onDataPass(data: Map<String, Any>)
    }

    fun setListener(listener: OnDataPass) {
        mCallback = listener
    }

    open fun validate(): Boolean {
        return true
    }


}