package napier.henrik.honours

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.Parcelable
import android.os.Vibrator
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.widget.TextView
import napier.henrik.honours.UserManager.Companion.userData

class ActivitySession : AppCompatActivity() {
    private lateinit var recyclerView: RecyclerView
    private lateinit var viewAdapter: RecyclerView.Adapter<*>
    private lateinit var viewManager: LinearLayoutManager
    lateinit var timer: TextView
    lateinit var session: SessionInst

//    data class Set(val reps: Int, val repsCompleted: Int, val weight: Int)
//    data class Exercise(val name: String,  val sets: ArrayList<Set>)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sessions)
        session = intent.getParcelableExtra<SessionInst>("SESSION")
        viewManager = LinearLayoutManager(this)
        viewAdapter = AdapterExercise(session.exerciseInsts, this)

        timer = findViewById(R.id.sessTimerTextView)
        RestTimer().setTimer(120000, timer, getSystemService(Context.VIBRATOR_SERVICE) as Vibrator)
        RestTimer.currentTimer.start()

        recyclerView = findViewById(R.id.exerciseRecycler)
        recyclerView.layoutManager = viewManager
        recyclerView.adapter = viewAdapter
//        recyclerView.addOnItemClickListener(object : OnItemClickListener {
//            override fun onItemClicked(position: Int, view: View) {
//                RestTimer.currentTimer.cancel()
//                startActivityForResult(Intent(baseContext, ActivityExerciseExpanded::class.java).putExtra("EXERCISE", session.exerciseInsts[position]), 1)
//            }
//        })
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        var exInst: ExerciseInst? = data?.extras?.getParcelable("EXERCISE")
        if (exInst != null) {
            var pos = session.exerciseInsts.find { it.exercise == exInst.exercise }
            session.exerciseInsts[session.exerciseInsts.indexOf(pos!!)] = exInst
            recyclerView.adapter?.notifyDataSetChanged()
        }
        if (session.exerciseInsts.filter { it.sets.filter { set -> set.repsCompleted == 0 }.isEmpty() }.isEmpty()) {
            RestTimer.currentTimer.cancel()
            userData.programme!!.sessions.add(session)
            var fin = intent.putExtra("SESSION", session as Parcelable)
            setResult(2, fin)
            finish()

        }
        RestTimer().setTimer(timer, getSystemService(Context.VIBRATOR_SERVICE) as Vibrator)
        RestTimer.currentTimer.start()

    }

    override fun onBackPressed() {
        if (session.exerciseInsts.filter { it.sets.filter { set -> set.repsCompleted < set.repGoal }.isEmpty() }.isEmpty()) {
            userData.programme!!.sessions.add(session)
            RestTimer.currentTimer.cancel()
            var fin = intent.putExtra("SESSION", session as Parcelable)
            setResult(2, fin)
            finish()
//            super.onBackPressed()
        } else {

        }
    }

}
