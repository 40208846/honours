package napier.henrik.honours

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize
import java.io.Serializable

// One workout's instance of an exercise
@Parcelize
class ExerciseInst(val exercise: DataWrangler.ExerciseJSON, var sets: ArrayList<Set>) : Parcelable, Serializable {
    //    val joints: Int = 0 // Is this needed?
//    var intensity: Int = 0;
    // If reps above goal
    var changeLoad: Int = 0

    // Refer to db
    fun calculateVolumeLoad(): Float {
        var total = 0f
        sets.forEach { total += it.repsCompleted * it.weight }
        return total
    }

    //    fun calculateIntensity (volumeLoad : Float, reps:Int): Unit {
//    }
    fun adjustLoad() {
        if (sets.filter { it.repsCompleted > it.repGoal }.isEmpty()) {
            changeLoad++
        } else if (sets.filter { it.repsCompleted < it.repGoal }.isEmpty()) {
            changeLoad--
        }
    }
}

