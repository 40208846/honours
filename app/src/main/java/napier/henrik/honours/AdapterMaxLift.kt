package napier.henrik.honours

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*

class AdapterMaxLift(var myDataSet: ArrayList<Pair<Int, Float>>,
                     var exercises: Array<String>,
                     var showNew: Boolean)
    : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    val VIEW_TYPE_NEW = 0
    val VIEW_TYPE_EXISTS = 1


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        if (viewType == VIEW_TYPE_NEW) {
            val maxLiftEntry = LayoutInflater.from(parent.context)
                    .inflate(R.layout.recycler_new_max_lifts, parent, false) as View
            return NewMaxLiftViewHolder(maxLiftEntry)
        } else {
            val maxLiftEntry = LayoutInflater.from(parent.context)
                    .inflate(R.layout.recycler_max_lift, parent, false) as View
            return MaxLiftViewHolder(maxLiftEntry)
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        var offset: Int = when (showNew || position == myDataSet.size) {
            true -> 1
            false -> 0
        }
        if (getItemViewType(position) == 0 && showNew) {
            var newLift = holder as NewMaxLiftViewHolder
            newLift.liftSpinner.adapter = ArrayAdapter<String>(
                    newLift.context!!, R.layout.support_simple_spinner_dropdown_item, exercises)
            newLift.button.setOnClickListener {
                if (newLift.liftSpinner.selectedItem != null && newLift.maxLiftVal.text != null) {
                    var max: Float = newLift.maxLiftVal.text.toString().toFloat()
                    Log.d("MAX", max.toString())
                    val exPk = UserManager.exercises.values.find { it.fields.name == exercises[newLift.liftSpinner.selectedItemPosition] }?.pk
                            ?: 0
                    myDataSet.add(Pair(exPk, max))
//                        user!!.maxLifts[newLift.liftSpinner.selectedItemPosition]?.add(max)
                    newLift.maxLiftVal.setText("")
                    newLift.liftSpinner.setSelection(0)
                }
                notifyItemChanged(position)
//                    notifyDataSetChanged()
            }
        } else {
            var maxLift = holder as MaxLiftViewHolder
            maxLift.name.text = UserManager.exercises[myDataSet[position - offset].first]?.fields?.name
            maxLift.max.text = myDataSet[position - offset].second.toString()
            maxLift.button.setOnClickListener {
                //                    user!!.maxLifts[myDataSet[position - 1].first]?.remove(myDataSet[position - 1].second)
                myDataSet.removeAt(position - offset)
//                    notifyItemChanged(position)
                notifyDataSetChanged()
            }


        }
    }


    override fun getItemCount(): Int {
        if (myDataSet.size == 0 && showNew) {
            return 1
        } else if (myDataSet.size == 0 && !showNew) {
            return 0
        } else {
            return myDataSet.size + 1
        }
    }

    override fun getItemViewType(position: Int): Int {
        if (position == 0 && showNew) {
            return VIEW_TYPE_NEW
        } else {
            return VIEW_TYPE_EXISTS
        }
    }

    class NewMaxLiftViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var context: Context? = itemView.context
        var liftSpinner: Spinner = itemView.findViewById(R.id.maxLiftSpinner)
        var maxLiftVal: EditText = itemView.findViewById(R.id.maxWeightEditText)
        var button: ImageButton = itemView.findViewById(R.id.addMLButton)
    }

    class MaxLiftViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var name: TextView = itemView.findViewById(R.id.maxLiftNameTextView)
        var max: TextView = itemView.findViewById(R.id.maxWeightTextView)
        var button: ImageButton = itemView.findViewById(R.id.removeMLButton)
    }
}