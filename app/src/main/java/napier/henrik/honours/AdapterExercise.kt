package napier.henrik.honours

import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.os.Parcelable
import android.os.Vibrator
import android.support.v7.widget.CardView
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ProgressBar
import android.widget.TextView
import napier.henrik.honours.UserManager.Companion.userData

class AdapterExercise(private val myDataSet: ArrayList<ExerciseInst>, var context: Context) : RecyclerView.Adapter<AdapterExercise.ExerciseViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AdapterExercise.ExerciseViewHolder {
        val cardView = LayoutInflater.from(parent.context).inflate(R.layout.exercise_collapsed, parent, false) as CardView
        return ExerciseViewHolder(cardView)
    }

    override fun onBindViewHolder(holder: ExerciseViewHolder, position: Int) {
        val ex: ExerciseInst = myDataSet[position]
        holder.exerciseTitle.text = ex.exercise.fields.name

        val mContext = context as ActivitySession

        if (position > userData.programme!!.programme.sessions[userData.programme!!.sessions.size % userData.programme!!.programme.days].exercises.size - 1) {
            holder.itemView.setBackgroundColor(Color.GREEN)
        }

        holder.itemView.setOnClickListener {
            RestTimer.currentTimer.cancel()
            var myContext = context as ActivitySession
            myContext.startActivityForResult(Intent(myContext.baseContext, ActivityExerciseExpanded::class.java).putExtra("EXERCISE", myDataSet[position] as Parcelable), 1)

        }
        if (ex.sets.filter { it.repsCompleted == 0 }.isNotEmpty()) {
            var cSet = ex.sets.filter { it.repsCompleted == 0 }.first()
            var progress = ex.sets.indexOf(cSet)
            var text: String = cSet.repGoal.toString() + "x" + cSet.weight.toString() + "kg"
            holder.currentSet.text = text
            holder.progressBar.max = ex.sets.size
            holder.progressBar.min = 0
            holder.progressBar.progress = progress
            holder.button.setOnClickListener {
                var myContext: ActivitySession = context as ActivitySession
                RestTimer.currentTimer.cancel()
                RestTimer().setTimer(120000, myContext.timer, myContext.getSystemService(Context.VIBRATOR_SERVICE) as Vibrator)
                RestTimer.currentTimer.start()
//            setCount++
                cSet.repsCompleted = cSet.repGoal
                progress = ex.sets.filter { it.repsCompleted > 0 }.size
//            ex.sets.add(progress,cSet)
                if (progress < ex.sets.size) {
                    holder.progressBar.progress = progress
                    cSet = ex.sets[progress]
                    text = cSet.repGoal.toString() + "x" + cSet.weight.toString() + "kg"
                    holder.currentSet.text = text
                } else {
                    holder.progressBar.progress = holder.progressBar.max
                    holder.button.isEnabled = false
                    holder.currentSet.text = holder.itemView.resources.getString(R.string.ex_complete)
                    if (myDataSet.filter { it.sets.filter { it.repsCompleted >= it.repGoal }.isEmpty() }.isEmpty()) {
//                        var myContext = context as ActivitySession
                        userData.programme!!.sessions.add(myContext.session)
                        RestTimer.currentTimer.cancel()
                        var fin = myContext.intent.putExtra("USER", userData as Parcelable)
                        fin.putExtra("SESSION", myContext.session as Parcelable)
                        myContext.setResult(2, fin)
                        myContext.finish()
                    }
                }
            }
        } else {
            holder.progressBar.progress = holder.progressBar.max
            holder.button.isEnabled = false
            holder.currentSet.text = holder.itemView.resources.getString(R.string.ex_complete)
        }
    }

    override fun getItemCount() = myDataSet.size

    class ExerciseViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private var view: View = itemView
        var exerciseTitle: TextView = view.findViewById(R.id.exerciseTitle)
        var currentSet: TextView = view.findViewById(R.id.currentSet)
        var progressBar: ProgressBar = view.findViewById(R.id.sessionInstProgressBar)
        var button: Button = view.findViewById(R.id.button)
    }
}
