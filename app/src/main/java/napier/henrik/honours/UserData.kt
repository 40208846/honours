package napier.henrik.honours

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize
import java.io.Serializable
import java.util.*

@Parcelize
data class UserData(
        var name:String,
        var experienceLevel: Int,
        var maxLifts: MutableMap<Int, ArrayList<Pair<Date,Float>>>,
        var programme: ProgrammeInst?,
        var weight: Float,
        var age: Int,
        var exerciseLoad: ArrayList<Float>)
    : Parcelable, Serializable