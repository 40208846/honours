package napier.henrik.honours

import android.app.Activity
import android.content.Context
import android.os.Bundle
import android.os.Parcelable
import android.os.Vibrator
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.CardView
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.Toolbar
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.ImageButton
import android.widget.TextView

class ActivityExerciseExpanded : AppCompatActivity() {
    private lateinit var recyclerView: RecyclerView
    private lateinit var viewAdapter: RecyclerView.Adapter<*>
    private lateinit var viewManager: LinearLayoutManager
    private lateinit var currEx: ExerciseInst
    private lateinit var timer: TextView
    private lateinit var exTitle: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_exercise_expanded)

        var toolbar = findViewById<Toolbar>(R.id.exExToolbar)
        setSupportActionBar(toolbar)

        currEx = intent.getParcelableExtra<ExerciseInst>("EXERCISE")
        viewManager = LinearLayoutManager(this)
        viewAdapter = SetAdapter(currEx.sets, this)

        timer = findViewById(R.id.exTimerTextView)
        RestTimer().setTimer(timer, getSystemService(Context.VIBRATOR_SERVICE) as Vibrator)
        RestTimer.currentTimer.start()

        exTitle = findViewById(R.id.exExTextView)
        exTitle.text = currEx.exercise.fields.name

        recyclerView = findViewById(R.id.exExRecyclerView)
        recyclerView.layoutManager = viewManager
        recyclerView.adapter = viewAdapter

    }

    override fun onBackPressed() {
        RestTimer.currentTimer.cancel()
        intent.putExtra("EXERCISE", currEx as Parcelable)
        setResult(Activity.RESULT_OK, intent)
        finish()
        super.onBackPressed()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.menu_set_done -> {
                intent.putExtra("EXERCISE", currEx as Parcelable)
                setResult(Activity.RESULT_OK, intent)
                finish()
            }
        }
        return true
    }

    class SetAdapter(private val myDataSet: ArrayList<Set>, var context: Context) : RecyclerView.Adapter<SetAdapter.SetViewHolder>() {

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SetViewHolder {
            val cardView = LayoutInflater.from(parent.context).inflate(R.layout.recycler_set, parent, false) as CardView
            return SetViewHolder(cardView)
        }

        override fun onBindViewHolder(holder: SetViewHolder, position: Int) {
            val currSet: Set = myDataSet[position]
            if (currSet.repsCompleted > 0) {
                holder.repCount.isEnabled = false
                holder.doneButton.isEnabled = false
            }
            holder.repCount.setText(currSet.repGoal.toString())
            holder.setText.text = "x "
            holder.repWeight.setText(currSet.weight.toString())
            holder.doneButton.setOnClickListener {
                var myContext: ActivityExerciseExpanded = context as ActivityExerciseExpanded
                myDataSet[position].repsCompleted = holder.repCount.text.toString().toInt()
                myDataSet[position].weight = holder.repWeight.text.toString().toFloat()
                holder.repCount.isEnabled = false
                holder.repWeight.isEnabled = false
                holder.doneButton.isEnabled = false
                RestTimer.currentTimer.cancel()
                RestTimer().setTimer(120000, myContext.timer, myContext.getSystemService(Context.VIBRATOR_SERVICE) as Vibrator)
                RestTimer.currentTimer.start()
                if (position == myDataSet.size - 1) {
                    var fin = myContext.intent.putExtra("EXERCISE", myContext.currEx as Parcelable)
                    RestTimer.currentTimer.cancel()
                    myContext.setResult(Activity.RESULT_OK, fin)
                    myContext.finish()
                }
            }
        }


        override fun getItemCount() = myDataSet.size

        class SetViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
            private var view: View = itemView
            var repCount: EditText = view.findViewById(R.id.repCountEditText)
            var setText: TextView = view.findViewById(R.id.setText)
            var repWeight: EditText = view.findViewById(R.id.repWeightEditText)
            var doneButton: ImageButton = view.findViewById(R.id.setDone)

        }
    }
}
