//package napier.henrik.honours
//
////https://gist.github.com/arcadefire/1e3a95314fdbdd78fb211b099d6ec9da#file-recyclerviewextension-kt
//
//import android.support.v7.widget.RecyclerView
//import android.view.View
//
//interface OnItemClickListener {
//    fun onItemClicked(position: Int, view: View)
//}
//
//fun RecyclerView.addOnItemClickListener(onClickListener: OnItemClickListener) {
//    this.addOnChildAttachStateChangeListener(object : RecyclerView.OnChildAttachStateChangeListener {
//        override fun onChildViewDetachedFromWindow(view: View?) {
//            view?.setOnClickListener(null)
//        }
//
//        override fun onChildViewAttachedToWindow(view: View?) {
//            view?.setOnClickListener({
//                val holder = getChildViewHolder(view)
//                onClickListener.onItemClicked(holder.adapterPosition, view)
//            })
//        }
//    })
//}