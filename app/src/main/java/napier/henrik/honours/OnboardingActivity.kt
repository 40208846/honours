package napier.henrik.honours

import android.app.Activity
import android.os.Bundle
import android.os.Parcelable
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import android.support.v4.view.ViewPager
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Button
import android.widget.ImageButton
import android.widget.ImageView
import kotlinx.android.synthetic.main.activity_onboarding.*
import napier.henrik.honours.UserManager.Companion.userData
import java.util.*

class OnboardingActivity : AppCompatActivity(), OnboardingFragment.OnDataPass {

    /**
     * The [android.support.v4.view.PagerAdapter] that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * [android.support.v4.app.FragmentStatePagerAdapter].
     */
    private var mSectionsPagerAdapter: SectionsPagerAdapter? = null
    //    lateinit var dataPasser: OnboardingFragment.OnDataPass
    lateinit var exercises: ArrayList<DataWrangler.ExerciseJSON?>
    lateinit var ouef: OnboardingUserExperienceFragment
    lateinit var frags: List<OnboardingFragment>

    lateinit var toolbar: Toolbar

    lateinit var userName: String
    var userWeight = 0f
    var userAge = 0
    var userEx = 0
    lateinit var userMax: ArrayList<Pair<Int, Float>>
    var userDays = 0
    var userGoal = "Strength"

//    lateinit var userData: UserData

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_onboarding)

        toolbar = findViewById(R.id.onboardingToolbar)
        toolbar.title = "Let's get some details"

        var bundle = intent.getBundleExtra("BUNDLE")
        exercises = bundle.getParcelableArrayList<DataWrangler.ExerciseJSON?>("EXERCISES")

        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = SectionsPagerAdapter(supportFragmentManager)

        container.adapter = mSectionsPagerAdapter
        ouef = OnboardingUserExperienceFragment()
        ouef.arguments = bundle
        frags = listOf(OnboardingUserDetailsFragment(), ouef, OnboardingUserProgrammeFragment())
        for (item in frags) {
            item.setListener(this)
        }
        var indicators = listOf<ImageView>(findViewById(R.id.intro_indicator_0), findViewById(R.id.intro_indicator_1), findViewById(R.id.intro_indicator_2))
        indicators[0].setBackgroundResource(R.drawable.indicator_selected)
        container.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(state: Int) {
            }

            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {

            }

            override fun onPageSelected(position: Int) {
                for (indicator in indicators) {
                    indicator.setBackgroundResource(R.drawable.indicator_unselected)
                }
                indicators[position].setBackgroundResource(R.drawable.indicator_selected)
            }
        }
        )
        var nextButton = findViewById<ImageButton>(R.id.intro_btn_next)
        var prevButton = findViewById<ImageButton>(R.id.intro_btn_prev)
        var finishButton = findViewById<Button>(R.id.intro_btn_finish)
        nextButton.setOnClickListener {
            if (frags[container.currentItem].validate()) {
                if (container.currentItem > 0) {
                    prevButton.visibility = View.VISIBLE
                }
                if (container.currentItem == 1) {
                    var mutableMap: MutableMap<Int, ArrayList<Pair<Date, Float>>> = mutableMapOf()
                    for (item in userMax) {
                        mutableMap[item.first] = ArrayList<Pair<Date, Float>>()
                        mutableMap[item.first]!!.add(Pair(Date(System.currentTimeMillis()), item.second))
                    }
                    userData = UserData(userName, userEx, mutableMap, null, userWeight, userAge, ArrayList())
                    UserManager().chooseProgramme(userDays, userGoal)
                    (frags[2] as OnboardingUserProgrammeFragment).showProgramme(userData)
                    nextButton.visibility = View.GONE
                    finishButton.visibility = View.VISIBLE
                    toolbar.title = "Here's your programme"
                }
                container.setCurrentItem(container.currentItem + 1, true)

            }
        }

        finishButton.setOnClickListener {
            intent.putExtra("USER", userData as Parcelable)
            setResult(Activity.RESULT_OK, intent)
            var prefs = getSharedPreferences("napier.henrik.honours.prefs", 0)
            var editor = prefs.edit()
            editor.putBoolean("ONBOARDED", true)
            editor.apply()

            finish()
        }
        // Set up the ViewPager with the sections adapter.
    }

    override fun onAttachFragment(fragment: android.app.Fragment?) {
        super.onAttachFragment(fragment)
        if (fragment is OnboardingFragment) {
            var onboardingFragment: OnboardingFragment = fragment
            onboardingFragment.mCallback = this
        }
    }

    override fun onDataPass(data: Map<String, Any>) {
        for (item in data) {
            when (item.key) {
                "NAME" -> userName = item.value as String
                "AGE" -> userAge = item.value as Int
                "WEIGHT" -> userWeight = item.value as Float
                "EXPERIENCE" -> userEx = item.value as Int
                "MAX" -> userMax = item.value as ArrayList<Pair<Int, Float>>
                "DAYS" -> userDays = item.value as Int
                "GOAL" -> userGoal = item.value as String
            }

        }
    }


    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_onboarding, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        val id = item.itemId

        if (id == R.id.action_settings) {
            return true
        }

        return super.onOptionsItemSelected(item)
    }


    /**
     * A [FragmentPagerAdapter] that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    inner class SectionsPagerAdapter(fm: FragmentManager) : FragmentPagerAdapter(fm) {

        override fun getItem(position: Int): Fragment {
            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).
            return frags[position]
        }

        override fun getCount(): Int {
            // Show 3 total pages.
            return 3
        }

    }

}
