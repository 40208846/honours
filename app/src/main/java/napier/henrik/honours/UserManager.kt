package napier.henrik.honours

import android.content.Context
import android.util.Log
import java.io.*
import java.util.*
import kotlin.collections.ArrayList
import kotlin.math.floor

class UserManager {

    companion object {
        lateinit var userData: UserData
        var programmes = ArrayList<Programme>()
        lateinit var exercises: MutableMap<Int, DataWrangler.ExerciseJSON>
        lateinit var muscles: Map<Int, DataWrangler.MusclesJSON>
        var categories = mapOf<Int, String>(
                8 to "Arms",
                9 to "Legs",
                10 to "Abs",
                11 to "Chest",
                12 to "Back",
                13 to "Shoulders",
                14 to "Calves"
        )

        fun writeUser(context: Context) {
            var bo = ByteArrayOutputStream()
            var so = ObjectOutputStream(bo)
            so.writeObject(userData)
            so.flush()

            context.openFileOutput("user", Context.MODE_PRIVATE).use {
                it.write(bo.toByteArray())
            }
        }

        fun readUser(context: Context) {
            try {
                context.openFileInput("user").use {

                    var bi = ByteArrayInputStream(it.readBytes())
                    var oi = ObjectInputStream(bi)
                    userData = oi.readObject() as UserData
                }
            } catch (e: FileNotFoundException) {
                Log.d("ReadUser", e.toString())
            }
        }
    }


    init {
        // Few main lifts
        programmes.add(Programme("Two Days", 2,
                arrayListOf(
                        Session(arrayOf(192, 119, 109, 81)),
                        Session(arrayOf(111, 105, 308))), 0))
        programmes.add(Programme("Three day PPL", 3,
                arrayListOf(
                        Session(arrayOf(192, 88, 229, 148)),
                        Session(arrayOf(105, 109, 207, 362)),
                        Session(arrayOf(111, 191, 346, 308))), 0))
        programmes.add(Programme("4 Dayz", 4,
                arrayListOf(
                        Session(arrayOf(105, 109, 207, 362)),
                        Session(arrayOf(111, 191, 346, 308)),
                        Session(arrayOf(192, 119, 109, 81)),
                        Session(arrayOf(111, 191, 346, 308))), 0))
        programmes.add(Programme("5 x 5", 5,
                arrayListOf(
                        Session(arrayOf(192, 88, 229, 148)),
                        Session(arrayOf(105, 109, 207, 362)),
                        Session(arrayOf(111, 191, 346, 308)),
                        Session(arrayOf(163, 123, 148, 237)),
                        Session(arrayOf(192, 119, 109, 81))), 0))
        programmes.add(Programme("666", 6,
                arrayListOf(
                        Session(arrayOf(192, 88, 229, 148)),
                        Session(arrayOf(105, 109, 207, 362)),
                        Session(arrayOf(111, 191, 346, 308)),
                        Session(arrayOf(163, 123, 148, 237)),
                        Session(arrayOf(109, 181, 362, 74)),
                        Session(arrayOf(111, 191, 346, 308))), 0))
    }

    fun chooseProgramme(days: Int, goal: String) {
        val userGoal: Int = when (goal) {
            "Strength" -> 0
            "Hypertrophy" -> 1
            else -> 0
        }
        val trainingLoad: Float = when (userData.experienceLevel) {
            0 -> 0.6f
            1 -> 0.7f
            2 -> 0.9f
            else -> {
                0.6f
            }
        }
        val restTime: Int = when (userData.experienceLevel) {
            0 -> 180
            1 -> 120
            2 -> 90
            else -> {
                120
            }
        }
        var sessions = ArrayList<Session>()
        when (days) {
            2 -> {
                sessions.add(createSession(arrayListOf(8, 10, 13), userData.experienceLevel))
                sessions.add(createSession(arrayListOf(9, 14, 12), userData.experienceLevel))
            }
            3 -> {
                sessions.add(createSession(arrayListOf(8, 10, 13), userData.experienceLevel))
                sessions.add(createSession(arrayListOf(9, 14, 12), userData.experienceLevel))
                sessions.add(createSession(arrayListOf(9, 14, 12), userData.experienceLevel))
            }
            4 -> {
                sessions.add(createSession(arrayListOf(8, 10, 13), userData.experienceLevel))
                sessions.add(createSession(arrayListOf(9, 14, 12), userData.experienceLevel))
                sessions.add(createSession(arrayListOf(8, 10, 13), userData.experienceLevel))
                sessions.add(createSession(arrayListOf(9, 14, 12), userData.experienceLevel))
            }
            5 -> {
                sessions.add(createSession(arrayListOf(8, 10, 13), userData.experienceLevel))
                sessions.add(createSession(arrayListOf(9, 14, 12), userData.experienceLevel))
                sessions.add(createSession(arrayListOf(8, 10, 13), userData.experienceLevel))
                sessions.add(createSession(arrayListOf(9, 14, 12), userData.experienceLevel))
                sessions.add(createSession(arrayListOf(9, 14, 12), userData.experienceLevel))
            }
            6 -> {
                sessions.add(createSession(arrayListOf(8, 10, 13), userData.experienceLevel))
                sessions.add(createSession(arrayListOf(9, 14, 12), userData.experienceLevel))
                sessions.add(createSession(arrayListOf(8, 10, 13), userData.experienceLevel))
                sessions.add(createSession(arrayListOf(9, 14, 12), userData.experienceLevel))
                sessions.add(createSession(arrayListOf(8, 10, 13), userData.experienceLevel))
                sessions.add(createSession(arrayListOf(9, 14, 12), userData.experienceLevel))
            }
        }
        programmes.add(Programme(String.format("Generated %d days", days), days, sessions, 0))

        val selection = programmes.filter { it.days == days }[0]

        selection.goal = userGoal

        userData.programme = ProgrammeInst(selection, trainingLoad, restTime, ArrayList())
    }

    fun createSessionInst(): SessionInst {
        var exs = ArrayList<ExerciseInst>()
        val sessEx = userData.programme!!.programme.sessions[userData.programme!!.sessions.size % userData.programme!!.programme.days].exercises
        val sessSugg = userData.programme!!.programme.sessions[userData.programme!!.sessions.size % userData.programme!!.programme.days].suggestions
        val sessExJ = exercises.values.filter { sessEx.contains(it.pk) }
        val repGoal = when (userData.programme!!.programme.goal) {
            0 -> 5
            1 -> 12
            else -> 5
        }

        for (item in sessExJ) {
            val weights = userData.maxLifts[item.pk]
            val weight = weights?.sortedBy { it.first }?.last()
            exs.add(ExerciseInst(item, arrayListOf(
                    Set(weightGen((weight?.second ?: 20f)
                            * userData.programme!!.trainingLoad * 0.8f
                    ), repGoal, 0),
                    Set(weightGen((weight?.second ?: 20f)
                            * userData.programme!!.trainingLoad * 0.9f
                    ), repGoal, 0),
                    Set(weightGen((weight?.second ?: 20f)
                            * userData.programme!!.trainingLoad
                    ), repGoal, 0),
                    Set(weightGen((weight?.second ?: 20f)
                            * userData.programme!!.trainingLoad * 0.8f
                    ), repGoal, 0))))
        }
        if (sessSugg[0] > 0) {
            val sessSuggJ = exercises.values.filter { sessSugg.contains(it.pk) }
            for (item in sessSuggJ) {
                val weights = userData.maxLifts[item.pk]
                val weight = weights?.sortedBy { it.first }?.last()
                exs.add(ExerciseInst(item, arrayListOf(
                        Set(weightGen((weight?.second ?: 20f) * userData.programme!!.trainingLoad
                                * 0.8f
                        ), repGoal, 0),
                        Set(weightGen((weight?.second ?: 20f)
                                * userData.programme!!.trainingLoad
                                * 0.9f
                        ), repGoal, 0),
                        Set(weightGen((weight?.second ?: 20f)
                                * userData.programme!!.trainingLoad
                        ), repGoal, 0),
                        Set(weightGen((weight?.second ?: 20f)
                                * userData.programme!!.trainingLoad
                                * 0.8f
                        ), repGoal, 0))))
            }
        }
//            userData.programme!!.sessions.add(SessionInst(exs))
        return SessionInst(exs)

    }

    // Create from categories and experience
    fun createSession(categories: ArrayList<Int>, experience: Int): Session {
        val exByCat = exercises.filter { categories.contains(it.value.fields.category) }
        return Session(exByCat.keys.toTypedArray())
    }

    fun sessionComplete(sessionInst: SessionInst) {
        // End of session, increase in load be applied when the individual can perform the current workload for one to two repetitions over the desired number on two consecutive training sessions
        val sessions = userData.programme!!.sessions.sortedBy { it.date }
        for (ex in sessionInst.exerciseInsts) {
            val prevSets = sessions.last().exerciseInsts.find { it.exercise == ex.exercise }?.sets
            val prevMax: Set? = prevSets?.maxBy { it.weight }
            val sessMax = ex.sets.maxBy { it.weight }
            if (sessMax != null && prevMax != null && sessMax.weight >= prevMax.weight) {
                if (sessMax.repsCompleted > sessMax.repGoal && prevMax.repsCompleted > prevMax.repGoal) {
                    //increase
                    userData.maxLifts[ex.exercise.pk]?.add(Pair(Date(System.currentTimeMillis()), prevMax.weight * 1.1f))
                } else if (sessMax.repsCompleted < sessMax.repGoal && prevMax.repsCompleted < prevMax.repGoal) {
                    userData.maxLifts[ex.exercise.pk]?.add(Pair(Date(System.currentTimeMillis()), prevMax.weight * 1.1f))
                } else {
                    userData.maxLifts[ex.exercise.pk]?.add(Pair(Date(System.currentTimeMillis()), prevMax.weight))
                }

            } else if (sessMax != null && prevMax != null && sessMax.weight < prevMax.weight) {
                if (sessMax.repsCompleted < sessMax.repGoal && prevMax.repsCompleted < prevMax.repGoal) {
                    userData.maxLifts[ex.exercise.pk]?.add(Pair(Date(System.currentTimeMillis()), prevMax.weight * 0.9f))
                } else {
                    userData.maxLifts[ex.exercise.pk]?.add(Pair(Date(System.currentTimeMillis()), prevMax.weight))
                }

            }
        }
        checkImbalance(userData)
    }

    fun weightGen(input: Float): Float {
        val weight = floor((input + 2.5 / 2) / 2.5) * 2.5
        return weight.toFloat()
    }

    fun checkImbalance(userData: UserData) {
        var mutableMap = mutableMapOf<Int, Float>()
        for (i in 1..16) {
            mutableMap[i] = 0f
        }
        userData.programme!!.sessions.forEach { sessionInst: SessionInst ->
            sessionInst.exerciseInsts.forEach { exerciseInst: ExerciseInst ->
                exerciseInst.exercise.fields.muscles.forEach { muscle: Int ->
                    mutableMap.put(muscle, mutableMap[muscle]!!.toFloat() + exerciseInst.calculateVolumeLoad())
                }
            }
        }
        var front = 0f
        var back = 0f
        for (muscle in mutableMap.keys) {
            when (muscles[muscle]!!.fields.is_front) {
                true -> front += mutableMap[muscle]!!
                else -> back += mutableMap[muscle]!!
            }
        }

        var nextSession = userData.programme!!.programme.sessions[userData.programme!!.sessions.size % userData.programme!!.programme.days]
        val musclesSess = exercises.values.filter { nextSession.exercises.contains(it.pk) }.map { it.fields.muscles }
        if (front / back > 10) {
            // do more back

            nextSession.suggestions = exercises.values.filter { !nextSession.exercises.contains(it.pk) && musclesSess.contains(it.fields.muscles) && it.fields.muscles.filter { muscle -> muscles[muscle]!!.fields.is_front }.isEmpty() }.map { it.pk }.toTypedArray()

        } else if (back / front > 10) {
            // do more front
            nextSession.suggestions = exercises.values.filter { !nextSession.exercises.contains(it.pk) && musclesSess.contains(it.fields.muscles) && it.fields.muscles.filter { muscle -> !muscles[muscle]!!.fields.is_front }.isEmpty() }.map { it.pk }.toTypedArray()

        } else {
            //balanced
        }
    }

}
