package napier.henrik.honours

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize
import java.io.Serializable

// Class will be used to generate sessions given certain parameters
@Parcelize
class Session(var exercises: Array<Int>) : Parcelable, Serializable {

    var suggestions: Array<Int> = arrayOf(0)

    // Will return SessionInst containing exercises to be completed
//    fun generate(exercises: MutableMap<Int, DataWrangler.ExerciseJSON>, user: UserData): SessionInst {
//
//        var exerciseInsts = arrayListOf<ExerciseInst>()
//        var volume = 0f
//        for (i in muscles) {
//            exercises.filter { entry -> entry.value.fields.muscles.contains(i) }.forEach { k, ex ->
//                if (volume < user.exerciseLoad[user.exerciseLoad.size - 1]) {
//                    var sets = arrayListOf<Set>()
//                    var max = user.maxLifts[k]?.max() ?: 100f
//                    sets.add(Set(max * trainingLoad, 5, 0))
//                    // Should be volume * reps
//                    volume += 50
//                    sets.add(Set(max * trainingLoad, 5, 0))
//                    volume += 50
//                    sets.add(Set(max * trainingLoad, 5, 0))
//                    volume += 50
//                    exerciseInsts.add(ExerciseInst(ex, sets))
//                }
//            }
//            exerciseInsts.sortBy { it.exercise.fields.muscles.size }
//
//        }
//
//        return SessionInst(exerciseInsts, muscles.toString())
//
//    }
}
