package napier.henrik.honours

import android.content.Intent
import android.os.Bundle
import android.os.Parcelable
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.widget.TextView
import napier.henrik.honours.UserManager.Companion.userData
import java.util.*

class ActivityMain : AppCompatActivity() {
    private lateinit var recyclerView: RecyclerView
    private lateinit var viewAdapter: RecyclerView.Adapter<*>
    private lateinit var viewManager: LinearLayoutManager
    lateinit var exerciseJSONs: MutableMap<Int, DataWrangler.ExerciseJSON>

//    data class Set(val reps: Int, val repsCompleted: Int, val weight: Int)
//    data class Exercise(val name: String,  val sets: ArrayList<Set>)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

//        var user = UserData(0, ArrayList(), "nSuns", 76.5f, 21, 50f)

//        val exercises = ArrayList<ExerciseInst>()
//
//        exercises.add(ExerciseInst("Bench", arrayListOf(Set(10.toFloat(),20,0),Set(15.toFloat(),25,0))))
//        exercises.add(ExerciseInst("OHP", arrayListOf(Set(5.toFloat(),10,0),Set(5.toFloat(),20,15),Set(5.toFloat(),20,0),Set(5.toFloat(),15,0))))


        var json = assets.open("exercises.json").bufferedReader().use { it.readText() }
        var mjson = assets.open("muscles.json").bufferedReader().use { it.readText() }

        exerciseJSONs = DataWrangler().parseJSON(json) ?: mutableMapOf()
        var muscleJSONs = DataWrangler().parseMuscles(mjson)


        if (exerciseJSONs.isNotEmpty()) {

            UserManager.exercises = exerciseJSONs
            UserManager.muscles = muscleJSONs.toMap()

//            //TODO: Replace section with proper user data access
//            var maxs = mutableMapOf<Int, ArrayList<Float>>()
//            var sessions = ArrayList<SessionInst>()
//            var user = UserData(0, maxs, Programme("Programme", 2), sessions, 150f, 21, arrayListOf(1000f))
//
//            var programmeInst = ProgrammeInst(user.programme)
//
//            var days = UserManager().chooseProgramme(user, 3)
//            for (day: Session in days) {
//                user.sessions.add(day.generate(attempt, user))
//            }
////            var today = days[0].generate(attempt, user)
//            programmeInst.sessions = sessions

            var exAL = ArrayList(exerciseJSONs.values)
            var data = Bundle()
            data.putParcelableArrayList("EXERCISES", exAL as java.util.ArrayList<out Parcelable?>)
//            data.putParcelable("USER", user)

            val prefs = getSharedPreferences("napier.henrik.honours.prefs", 0)
            if (!prefs.getBoolean("ONBOARDED", false)) {
                startActivityForResult(Intent(baseContext, OnboardingActivity::class.java).putExtra("BUNDLE", data), 1)

            } else {

                //Try to get user from file
                UserManager.readUser(this)
                userData.maxLifts[192] = arrayListOf(
                        Pair(Date(2018 - 1900, 11, 2), 80f),
                        Pair(Date(2018 - 1900, 11, 2), 80f),
                        Pair(Date(2018 - 1900, 11, 4), 82f),
                        Pair(Date(2018 - 1900, 11, 6), 85f),
                        Pair(Date(2018 - 1900, 11, 8), 87f),
                        Pair(Date(2018 - 1900, 11, 14), 90f),
                        Pair(Date(2018 - 1900, 11, 19), 92f),
                        Pair(Date(2018 - 1900, 11, 21), 90f),
                        Pair(Date(2018 - 1900, 11, 25), 92f),
                        Pair(Date(2018 - 1900, 12, 2), 93f),
                        Pair(Date(2018 - 1900, 12, 5), 91f),
                        Pair(Date(2018 - 1900, 12, 9), 90f),
                        Pair(Date(2018 - 1900, 12, 12), 91f),
                        Pair(Date(2018 - 1900, 12, 15), 95f),
                        Pair(Date(2018 - 1900, 12, 18), 97f),
                        Pair(Date(2018 - 1900, 12, 22), 95f),
                        Pair(Date(2018 - 1900, 12, 28), 92f),
                        Pair(Date(2019 - 1900, 1, 2), 90f),
                        Pair(Date(2019 - 1900, 1, 7), 90f),
                        Pair(Date(2019 - 1900, 1, 12), 93f),
                        Pair(Date(2019 - 1900, 1, 15), 92f),
                        Pair(Date(2019 - 1900, 1, 20), 95f))
                userData.maxLifts[119] = arrayListOf(Pair(Date(2019 - 1900, 2, 22), 40f))
                userData.maxLifts[88] = arrayListOf(Pair(Date(2019 - 1900, 2, 22), 50f))
                userData.maxLifts[229] = arrayListOf(Pair(Date(2019 - 1900, 2, 22), 40f))
                userData.maxLifts[148] = arrayListOf(Pair(Date(2019 - 1900, 2, 22), 12f))
                userData.maxLifts[105] = arrayListOf(Pair(Date(2019 - 1900, 2, 22), 140f))
                userData.maxLifts[109] = arrayListOf(Pair(Date(2019 - 1900, 2, 22), 70f))
                userData.maxLifts[207] = arrayListOf(Pair(Date(2019 - 1900, 2, 22), 35f))
                userData.maxLifts[362] = arrayListOf(Pair(Date(2019 - 1900, 2, 22), 24f))
                userData.maxLifts[111] = arrayListOf(Pair(Date(2019 - 1900, 2, 22), 110f))
                userData.maxLifts[191] = arrayListOf(Pair(Date(2019 - 1900, 2, 22), 60f))
                userData.maxLifts[346] = arrayListOf(Pair(Date(2019 - 1900, 2, 22), 40f))
                userData.maxLifts[308] = arrayListOf(Pair(Date(2019 - 1900, 2, 22), 170f))
                userData.maxLifts[163] = arrayListOf(Pair(Date(2019 - 1900, 2, 22), 55f))
                userData.maxLifts[123] = arrayListOf(Pair(Date(2019 - 1900, 2, 22), 36f))
                userData.maxLifts[148] = arrayListOf(Pair(Date(2019 - 1900, 2, 22), 12f))
                userData.maxLifts[237] = arrayListOf(Pair(Date(2019 - 1900, 2, 22), 16f))
                userData.maxLifts[181] = arrayListOf(Pair(Date(2019 - 1900, 2, 22), 10f))
                userData.maxLifts[74] = arrayListOf(Pair(Date(2019 - 1900, 2, 22), 24f))


                gotUser()
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        UserManager.writeUser(this)
        gotUser()
        super.onActivityResult(requestCode, resultCode, data)
    }


    fun gotUser() {
        val textView = findViewById<TextView>(R.id.welcomeTextView)
        textView.text = "Welcome, " + userData.name
        //TODO: Application code here
//        var newSession = UserManager.createSessionInst()
//        var button: MaterialButton = findViewById(R.id.todayButton)
//        button.text = "New day"
//        button.setOnClickListener {
//            // Could be done more intelligently
//            var sessionActivity = Intent(baseContext, ActivitySession::class.java)
//            sessionActivity.putExtra("SESSION", newSession as Parcelable)
//            startActivityForResult(sessionActivity, 2)

//        }

//        if (userData.maxLifts.isEmpty()) {
//
//        }
//        var graph = findViewById<GraphView>(R.id.maxLiftGraph)
//        var pointsAl = arrayListOf<DataPoint>()
//        if (userData.programme!!.sessions.size > 0) {
//            userData.programme!!.sessions.forEachIndexed { index, pair ->
//                pointsAl.add(DataPoint(index.toDouble(), pair.exerciseInsts.map { it.calculateVolumeLoad() }.sum().toDouble()))
//            }
//            val points: Array<DataPoint> = pointsAl.toTypedArray()
//
//            var series = LineGraphSeries(points)
//            graph.addSeries(series)
//            graph.gridLabelRenderer.labelFormatter = DateAsXAxisLabelFormatter(this)
//            graph.gridLabelRenderer.numHorizontalLabels = 3
//            graph.viewport.setMaxY(points.maxBy { it.y }!!.y + 100f)
//            graph.viewport.setMinY(0f.toDouble())
//            graph.viewport.setMinX(points[0].x)
//            graph.viewport.setMaxX(points[points.size - 1].x)
//            graph.viewport.isXAxisBoundsManual = true
//            graph.gridLabelRenderer.setHumanRounding(false, true)
//        }

        recyclerView = findViewById(R.id.mainRecyclerView)
        viewManager = LinearLayoutManager(this)
//        var maxLifts: ArrayList<Pair<Int, Float>> = arrayListOf()
//        for (lift in userData.maxLifts) {
//            maxLifts.add(Pair(lift.key, lift.value.maxBy { it.second }!!.second))
//        }
//        val exArray = exerciseJSONs.values.map { it.fields.name }
        viewAdapter = AdapterSessionInst(userData.programme!!.sessions)
        recyclerView.layoutManager = viewManager
        recyclerView.adapter = viewAdapter


    }

    override fun onPause() {
        var prefs = getSharedPreferences("napier.henrik.honours.prefs", 0)
        if (prefs.getBoolean("ONBOARDED", false)) {
            UserManager.writeUser(this)
        }
        super.onPause()
    }

    override fun onResume() {
        var prefs = getSharedPreferences("napier.henrik.honours.prefs", 0)
        if (prefs.getBoolean("ONBOARDED", false)) {
            UserManager.readUser(this)
        }
        super.onResume()
    }
}
