package napier.henrik.honours

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize
import java.io.Serializable

@Parcelize
class ProgrammeInst(var programme: Programme,
                    var trainingLoad: Float = 0f,
                    var restTime: Int = 0,
                    var sessions: ArrayList<SessionInst> = ArrayList()):Parcelable,Serializable