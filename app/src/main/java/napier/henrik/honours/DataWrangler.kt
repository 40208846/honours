package napier.henrik.honours

import android.os.Parcelable
import com.squareup.moshi.Moshi
import kotlinx.android.parcel.Parcelize
import java.io.Serializable

// Class to get data about exercises in usable format
class DataWrangler {

    @Parcelize
    data class FieldsJSON(
            val license_author: String,
            val status: Int,
            val language: Int,
            val muscles_secondary: List<Int>,
            val description: String,
            val equipment: List<Int>,
            val creation_date: String?,
            val category: Int,
            val uuid: String,
            val muscles: List<Int>,
            val license: Int,
            val name: String
    ) : Parcelable, Serializable

    @Parcelize
    data class ExerciseJSON(
            val pk: Int,
            val fields: FieldsJSON,
            val model: String
    ) : Parcelable, Serializable

    @Parcelize
    data class MusclesJSON(
            val pk: Int,
            val model: String,
            val fields: MFieldsJSON
    ) : Parcelable, Serializable

    @Parcelize
    data class MFieldsJSON(
            val is_front: Boolean,
            val name: String
    ) : Parcelable, Serializable


    fun parseJSON(json: String): MutableMap<Int, ExerciseJSON>? {
        val moshi = Moshi.Builder().build()
        val jsonAdapter = moshi.adapter<Array<ExerciseJSON>>(Array<ExerciseJSON>::class.java).nullSafe()
        var exercises = mutableMapOf<Int, ExerciseJSON>()

        try {
            var exArray = jsonAdapter.fromJson(json)
            exArray?.forEach { if (it.fields.language == 2 && it.fields.status == 2) exercises[it.pk] = it }
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return exercises
    }

    fun parseMuscles(json: String): Map<Int, MusclesJSON> {
        val moshi = Moshi.Builder().build()
        val jsonAdapter = moshi.adapter<Array<MusclesJSON>>(Array<MusclesJSON>::class.java).nullSafe()
        var exercises = mutableMapOf<Int, MusclesJSON>()

        try {
            var exArray = jsonAdapter.fromJson(json)
            exArray?.forEach { exercises[it.pk] = it }
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return exercises.toMap()
    }
}
