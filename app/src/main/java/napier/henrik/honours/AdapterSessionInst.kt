package napier.henrik.honours

import android.content.Intent
import android.os.Parcelable
import android.support.v7.widget.CardView
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import java.text.SimpleDateFormat
import java.util.*

class AdapterSessionInst(private val myDataSet: ArrayList<SessionInst>) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    val VIEW_TYPE_NEW = 0
    val VIEW_TYPE_EXISTS = 1

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        var cardView: CardView
        when (viewType) {
            VIEW_TYPE_NEW -> {
                cardView = LayoutInflater.from(parent.context).inflate(R.layout.session_new, parent, false) as CardView
                return NewSessionInstViewHolder(cardView, parent.context as ActivityMain)
            }
            else -> {
                cardView = LayoutInflater.from(parent.context).inflate(R.layout.session_collapsed, parent, false) as CardView
                return SessionInstViewHolder(cardView)
            }
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (position) {
            0 -> holder.itemView.setOnClickListener { v ->
                var newSession = holder as NewSessionInstViewHolder
                var newSess = UserManager().createSessionInst()
                var sessionActivity = Intent(newSession.myContext, ActivitySession::class.java).putExtra("SESSION", newSess as Parcelable)
                newSession.myContext.startActivityForResult(sessionActivity, 2)
            }
            else -> {
                val sessHolder = holder as SessionInstViewHolder
                val sess: SessionInst = myDataSet[position - 1]
                val df = SimpleDateFormat("EEEE, dd/MM/yyyy hh:mm", Locale.getDefault())
                sessHolder.sessionTitle.text = df.format(sess.date)
                var desc = StringBuilder()
                sess.exerciseInsts.forEach { desc.append(it.exercise.fields.name, "\t", it.sets.maxBy { set -> set.weight }!!.weight, "kg\n") }
                sessHolder.sessionDesc.text = desc
//        holder.progressBar.progress =
                holder.itemView.setOnClickListener { v ->
                    {
                    }
                }
            }
        }
    }


    override fun getItemCount(): Int {
        when (myDataSet.size) {
            0 -> return 1
            else -> return myDataSet.size + 1
        }
    }

    override fun getItemViewType(position: Int): Int {
        when (position) {
            0 -> return VIEW_TYPE_NEW
            else -> return VIEW_TYPE_EXISTS
        }
    }

    class NewSessionInstViewHolder(itemView: View, val myContext: ActivityMain) : RecyclerView.ViewHolder(itemView)

    class SessionInstViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private var view: View = itemView
        var sessionTitle: TextView = view.findViewById(R.id.sessionInstTitle)
        var sessionDesc: TextView = view.findViewById(R.id.sessionInstDesc)
//        var button: Button = view.findViewById(R.id.button)

    }
}

